# CMC Markets - Simple web shopping page

“It should allow them to add and remove items to/from a virtual shopping basket. The page should also have a “check out” option 
that presents the user with various delivery options to select from.  Once selected, a summary of the order + delivery method and 
charges should be presented.  On acceptance, a “thank you” page should be shown.  At any point in the process the user should be 
able to step back and amend their selections.

## Table of Contents

- [Installation](#installation)
- [Running](#running)

## Installation

You can install though using npm or yarn using the following commands:
```bash
# using npm
npm i

# or using yarn
yarn
```

## Running

To start the application on a dev server, you can run the npm script start: 
```bash
# using npm
npm start

# or using yarn
yarn start
```

When the application it will open a browser automatically on localhost 
(default port 3000). 
