const EventType = {
    SET : 'shared.actions.cart.set',
    ADD : 'shared.actions.cart.add',
    REMOVE : 'shared.actions.cart.remove',
    CLEAR : 'shared.actions.cart.clear',
};

export default EventType;