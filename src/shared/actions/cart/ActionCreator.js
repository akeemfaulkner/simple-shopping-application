import {createAction} from '../../utlis';
import EventType from "./EvenType";

export const add = createAction(EventType.ADD);
export const set = (sku, qty) => createAction(EventType.SET)({sku, qty});
export const remove = createAction(EventType.REMOVE);
export const clear = createAction(EventType.CLEAR);
