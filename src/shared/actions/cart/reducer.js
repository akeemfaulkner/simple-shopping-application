import BaseReducer from '../../BaseReducer';
import EventType from "./EvenType";


export default class Reducer extends BaseReducer {
    constructor() {
        super(getDefaultState);

        this.handlers = {
            [EventType.ADD]: this.handleAdd.bind(this),
            [EventType.SET]: this.handleSet.bind(this),
            [EventType.REMOVE]: this.handleRemove.bind(this),
            [EventType.CLEAR]: this.resetState.bind(this),
        };
    }

    handleAdd(state, {payload: sku}) {
        let nextState = {...state};
        let product = nextState[sku];
        nextState[sku] = product ? product + 1 : 1;
        return nextState;
    }

    handleSet(state, {payload}) {
        let nextState = {...state};
        nextState[payload.sku] = parseInt(payload.qty, 10);
        return nextState;
    }

    handleRemove(state, {payload: sku}) {
        let nextState = {...state};
        delete nextState[sku];
        return nextState;
    }

}

export function getDefaultState() {
    return {};
}

