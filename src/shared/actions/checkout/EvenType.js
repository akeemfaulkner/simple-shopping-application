const EventType = {
    ADD_DELIVERY : 'shared.actions.checkout.addDelivery',
    CLEAR_DELIVERY : 'shared.actions.checkout.clearDelivery',
    UPDATE_DELIVERY_METHOD : 'shared.actions.checkout.updateDeliveryMethod',
};

export default EventType;