import {createAction} from '../../utlis';
import EventType from "./EvenType";

export const updateDeliveryMethod = createAction(EventType.UPDATE_DELIVERY_METHOD);
export const addDelivery = createAction(EventType.ADD_DELIVERY);
export const clearDelivery = createAction(EventType.CLEAR_DELIVERY);
