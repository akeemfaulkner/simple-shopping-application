import BaseReducer from '../../BaseReducer';
import EventType from "./EvenType";


export default class Reducer extends BaseReducer {
    constructor() {
        super(getDefaultState);

        this.handlers = {
            [EventType.UPDATE_DELIVERY_METHOD]: this.handleUpdateDeliveryMethod.bind(this),
            [EventType.ADD_DELIVERY]: this.handleAddDelivery.bind(this),
            [EventType.CLEAR_DELIVERY]: this.resetState.bind(this)
        };
    }

    handleUpdateDeliveryMethod(state, {payload}) {
        let nextState = {...state};
        nextState.deliveryMethod = payload;
        return nextState;
    }

    handleAddDelivery(state, {payload}) {
        let nextState = {...state};
        return {...nextState, ...payload};
    }
}

export function getDefaultState() {
    let deliveryOptions = {
        Standard: {
            name: 'Standard 3-5 days',
            unit: 0
        },
        DPD: {
            name: 'Next day',
            unit: 2.99
        }
    };
    return {
        deliveryOptions: deliveryOptions,
        deliveryMethod: 'Standard',
        recipient: '',
        address: ''
    };
}

