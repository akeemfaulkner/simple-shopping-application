import BaseReducer from '../../BaseReducer';
import chinos from '../../../assets/chinos.jpg';
import shoes from '../../../assets/shoes.jpeg';
import suit from '../../../assets/suit.jpg';
import tshirt from '../../../assets/t-shirt.jpg';



export default class Reducer extends BaseReducer {
    constructor() {
        super(getDefaultState);
    }


}

export function getDefaultState() {
       return [
        {
            image: chinos,
            name: 'Chinos',
            sku: 'a',
            unit: 100
        },
        {
            image: shoes,
            name: 'Shoes',
            sku: 'b',
            unit: 180
        },
        {
            image: suit,
            name: 'Suit',
            sku: 'c',
            unit: 300
        },
        {
            image: tshirt,
            name: 'T-shirt',
            sku: 'd',
            unit: 50
        },
    ]
}

