import {createStore, applyMiddleware, compose, combineReducers} from 'redux';
import CartReducer from "./shared/actions/cart/reducer";
import ProductsReducer from "./shared/actions/products/reducer";
import CheckoutReducer from "./shared/actions/checkout/reducer";



let rootReducer = combineReducers({
    cart: new CartReducer().reduce,
    products: new ProductsReducer().reduce,
    checkout: new CheckoutReducer().reduce,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(...[])
));
export default store;
