import React, {Component} from 'react';
import {Route,   BrowserRouter as Router} from "react-router-dom";
import Landing from "./containers/Landing/Landing";
import Products from "./containers/Products/Products";
import Cart from "./containers/Header/Header";
import ShoppingCart from "./containers/ShoppingCart/ShoppingCart";

class App extends Component {
    render() {
        return (
            <Router>
                <div>
                    <Cart/>
                    <Route exact path="/" component={Landing}/>
                    <Route path="/products" component={Products}/>
                    <Route path="/shopping-cart" component={ShoppingCart}/>
                </div>
            </Router>
        );
    }
}

export default App;
