import React from 'react';
import {Link} from "react-router-dom";
import {connect} from "react-redux";

import cartIcon from '../../assets/cart.png';

import './Header.css';

export class Header extends React.Component {
    render() {
        let cartCount = this.getProductCount();
        return (
            <div className="header">
                <Link className="cart" to="/shopping-cart">
                    {cartCount ? <span className="cart__count">{cartCount}</span> : null}
                    <img src={cartIcon} className="cart__icon"/>
                </Link>
            </div>
        );
    }

    getProductCount() {
        let {cart} = this.props;
        return Object.values(cart)
            .reduce((previousValue, currentValue) => previousValue + currentValue, 0)
    }
}

export default connect(
    mapState
)(Header);


function mapState({cart}) {
    return {
        cart
    };
}