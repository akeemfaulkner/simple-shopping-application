import React, {Component} from 'react';
import {Link} from "react-router-dom";

import banner from '../../assets/banner.jpg';

class Landing extends Component {
    render() {
        return (
            <div className="hero">
                <img src={banner} className="hero__banner" alt="logo"/>
                <div className="hero__contents">
                    <div className="hero__header">
                        <div className="hero__title">Sale On Now!</div>
                        <Link to={`/products`} className="hero__cta">
                            Shop Now!
                        </Link>
                    </div>
                </div>
            </div>
        );
    }
}

export default Landing;
