import React, {Component} from 'react';
import {connect} from 'react-redux';

import './Confirm.css';

export class Confirm extends Component {

    constructor(props) {
        super(props);

        this.handleConfirm = this.handleConfirm.bind(this);

        this.state = {
            isConfirmed: false
        };

    }

    render() {
        const {cart, total, delivery, checkout, onClose} = this.props;

        return (
            <div className="confirm">
                <h1>Order Summary</h1>
                <p>Your order information is below:</p>
                <h4><b>Total Cost:</b> £{total}</h4>
                <h5><b>Delivery Option:</b> {delivery}</h5>
                <h5><b>Recipient:</b> {checkout.recipient}</h5>

                <div>
                    <b>Delivery Address:</b>
                    <br/>
                    <p dangerouslySetInnerHTML={{__html: checkout.address.replace(/\n\r?/g, '<br />')}}/>
                </div>

                <div><b>Your items:</b></div>
                <table className="checkout-table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>name</th>
                        <th>sku</th>
                        <th>price</th>
                        <th>qty</th>
                    </tr>
                    </thead>
                    <tbody>
                    {cart.map((item, index) => (
                        <tr key={`item-${index}-${item.product.name}`}>
                            <td><img src={item.product.image} alt={item.product.name}/></td>
                            <td>{item.product.name}</td>
                            <td>{item.sku}</td>
                            <td>£{item.qty * item.product.unit}</td>
                            <td>{item.qty}</td>
                        </tr>
                    ))}
                    </tbody>
                </table>
                <div className="form__row form__ctas">
                    <button className="button" type="button" onClick={onClose}>Back</button>
                    <button className="button" type="button" onClick={this.handleConfirm}>Confirm</button>
                </div>
                {this.state.isConfirmed ?
                    <div className="thankyou">
                        <h1>Thank you!</h1>
                    </div>
                    : null}
            </div>
        );
    }

    handleConfirm() {
        this.setState({isConfirmed: true});
        this.scrollToTop();
    }

    componentDidMount() {
        this.scrollToTop();
    }

    scrollToTop() {
        document.documentElement.scrollTop = 0;
    }

}

export default connect(
    ({checkout}) => ({checkout})
)(Confirm);

