import React, {Component} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import * as checkoutActions from "../../shared/actions/checkout/ActionCreator";

import './Delivery.css';

export class Delivery extends Component {

    constructor(props) {
        super(props);
        this.handleSetDeliveryMethod = this.handleSetDeliveryMethod.bind(this);
        this.setField = this.setField.bind(this);
        this.handleNext = this.handleNext.bind(this);
        this.handleClose = this.handleClose.bind(this);

        this.state = {
            recipient: '',
            address: ''
        };
    }

    render() {
        const {total, checkout: {deliveryMethod}} = this.props;
        const {recipient, address} = this.state;

        return (
            <div className="delivery-inner">
                <form className="form">
                    <div className="form__row">
                        <input type="text" placeholder="Recipient's name" onChange={this.setField('recipient')}
                               value={recipient}/>
                    </div>
                    <div className="form__row">
                        <label>Delivery Address</label>
                        <textarea
                            cols="30"
                            rows="10"
                            onChange={this.setField('address')}
                            value={address}
                        >
                            {address}
                            </textarea>
                    </div>


                    <div className="form__row">
                        <select defaultValue={deliveryMethod} onChange={this.handleSetDeliveryMethod}>
                            {this.renderDeliveryOptions()}
                        </select>
                    </div>
                    <div className="form__row">
                        <h3>Total costs (inc delivery): £{total + this.getDeliveryUnitPrice()}</h3>
                    </div>
                    <div className="form__row form__ctas">
                        <button className="button" type="button" onClick={this.handleClose}>Back</button>
                        <button className="button" type="button" disabled={!this.isFormValid()}
                                onClick={this.handleNext}>Next
                        </button>
                    </div>
                </form>
            </div>
        );
    }

    componentDidMount() {
        const {checkout: {recipient, address}} = this.props;
        this.setState({recipient, address});
    }

    handleClose() {
        const {actions: {checkout}, onClose} = this.props;
        checkout.addDelivery(this.state);
        onClose();
    }

    handleNext() {
        const {actions: {checkout}, onNext} = this.props;
        checkout.addDelivery(this.state);
        onNext();

    }

    handleSetDeliveryMethod(e) {
        const {actions: {checkout}} = this.props;
        checkout.updateDeliveryMethod(e.target.value);
    }

    renderDeliveryOptions() {
        const {checkout: {deliveryOptions}} = this.props;
        return Object.keys(deliveryOptions).map((option, index) => {
            const name = deliveryOptions[option].name;
            const price = deliveryOptions[option].unit;
            return <option key={`option-${option}-${index}`} value={option}>{name} - £{price}</option>
        });
    }

    isFormValid() {
        for (let field in this.state) {
            if (!this.state[field].match(/\S+/gmi)) return false;
        }
        return true;
    }

    setField(fieldName) {
        return (e) => {
            this.setState({[fieldName]: e.target.value});
        };
    }

    getDeliveryUnitPrice() {
        const {checkout: {deliveryOptions, deliveryMethod}} = this.props;
        return deliveryOptions[deliveryMethod].unit;
    }

}

export default connect(
    mapState,
    mapDispatch
)(Delivery);

function mapState({products, cart, checkout}) {
    return {
        checkout,
        products,
        cart
    };
}

function mapDispatch(dispatch) {
    return {
        actions: {
            checkout: bindActionCreators(checkoutActions, dispatch)
        }
    };
}

