import React, {Component} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import { withRouter } from 'react-router'

import Product from "../../components/Product/Product";
import * as cartActions from "../../shared/actions/cart/ActionCreator";

import './Products.css';

export class Products extends Component {

    constructor(props) {
        super(props);
        this.handleAddToCart = this.handleAddToCart.bind(this);
        this.handleBuyNow = this.handleBuyNow.bind(this);
    }

    render() {

        return (
            <div className="products">
                {this.props.products.map((product, index) =>
                    (
                        <div key={`product-${index}`} className="products__single">
                            <Product
                                {...product}
                                onAddToCart={this.handleAddToCart(product.sku)}
                                onBuyNow={this.handleBuyNow(product.sku)}
                            />
                        </div>
                    )
                )}
            </div>
        );
    }

    handleAddToCart(sku) {
        const {actions: {cart: {add}}} = this.props;
        return () => add(sku);
    }

    handleBuyNow(sku) {
        const { history } = this.props;
        return () => {
            history.push('/shopping-cart');
            this.handleAddToCart(sku)();
        };
    }
}

export default withRouter(connect(
    mapState,
    mapDispatch
)(Products));

function mapState({products}) {
    return {
        products
    };
}

function mapDispatch(dispatch) {
    return {
        actions: {
            cart: bindActionCreators(cartActions, dispatch)
        }
    };
}

