import React, {Component} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {Link} from "react-router-dom";

import * as cartActions from "../../shared/actions/cart/ActionCreator";
import Delivery from "../Delivery/Delivery";
import Confirm from "../Confirm/Confirm";

import './ShoppingCart.css';

export class ShoppingCart extends Component {

    constructor(props) {
        super(props);
        this.handleAddToCart = this.handleAddToCart.bind(this);
        this.handleBuyNow = this.handleBuyNow.bind(this);
        this.handleRemoveItem = this.handleRemoveItem.bind(this);
        this.handleAddUpdateQty = this.handleAddUpdateQty.bind(this);
        this.toggleDelivery = this.toggleDelivery.bind(this);
        this.toggleConfirm = this.toggleConfirm.bind(this);

        this.state = {
            isDeliveryDisplayed: false,
            isConfirmDisplayed: false,
        };
    }

    render() {

        const cart = this.getCartContents();
        const totalCartCost = this.getTotalCartCost(cart);
        const {deliveryMethod} = this.props;
        const {isDeliveryDisplayed, isConfirmDisplayed} = this.state;

        return (
            <div className="checkout">
                <Link to={`/products`} className="checkout__cta">
                    Shop Now!
                </Link>
                <h1>Shopping Cart</h1>

                <h4>Total Cost: £{totalCartCost}</h4>
                <table className="checkout-table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>name</th>
                        <th>sku</th>
                        <th>price</th>
                        <th>qty</th>
                        <th>Remove</th>
                    </tr>
                    </thead>
                    <tbody>
                    {cart.map((item, index) => (
                        <tr key={`item-${index}-${item.product.name}`}>
                            <td><img src={item.product.image} alt={item.product.name}/></td>
                            <td>{item.product.name}</td>
                            <td>{item.sku}</td>
                            <td>£{item.qty * item.product.unit}</td>
                            <td><input type="number" defaultValue={item.qty}
                                       onChange={this.handleAddUpdateQty(item.sku)}/></td>
                            <td>
                                <button className="button" onClick={this.handleRemoveItem(item.sku)}>Remove</button>
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </table>
                <div className="checkout__ctas">
                    <button className="button" disabled={!cart.length} onClick={this.toggleDelivery}>
                        Check out
                    </button>
                </div>

                {isDeliveryDisplayed ?
                    <Delivery
                        total={totalCartCost}
                        onNext={this.toggleConfirm}
                        onClose={this.toggleDelivery}
                    />
                    : null}
                {isConfirmDisplayed ?
                    <Confirm
                        cart={cart}
                        total={totalCartCost + deliveryMethod.unit}
                        delivery={deliveryMethod.name}
                        onClose={this.toggleConfirm}
                    />
                    : null}

            </div>
        );
    }

    toggleDelivery() {
        const isDeliveryDisplayed = !this.state.isDeliveryDisplayed;
        this.setState({isDeliveryDisplayed})
    }

    toggleConfirm() {
        const isConfirmDisplayed = !this.state.isConfirmDisplayed;
        this.setState({isConfirmDisplayed})
    }

    getCartContents() {
        const {cart} = this.props;
        return Object.keys(cart).reduce((products, sku) => {
            const product = this.findProductBySku(sku);
            const content = {
                sku,
                product,
                qty: cart[sku]
            };
            return products.concat([content]);
        }, [])
    }


    getTotalCartCost(cartContents) {
        return cartContents.reduce((total, item) => {
            return total + (item.qty * item.product.unit);
        }, 0)
    }

    findProductBySku(sku) {
        const {products} = this.props;
        return products.find(product => product.sku === sku);
    }

    handleAddToCart(sku) {
        const {actions: {cart: {add}}} = this.props;
        return () => add(sku);
    }

    handleAddUpdateQty(sku) {
        const {actions: {cart: {set}}} = this.props;
        return (e) => {
            let value = e.target.value;
            if (value <= 0) return this.handleRemoveItem(sku)();
            set(sku, value);
        }
    }

    handleBuyNow(sku) {
        return () => this.handleAddToCart(sku)();
    }

    handleRemoveItem(sku) {
        const {actions: {cart: {remove}}} = this.props;
        return () => remove(sku);
    }
}

export default connect(
    mapState,
    mapDispatch
)(ShoppingCart);

function mapState({products, cart, checkout}) {
    return {
        products,
        cart,
        deliveryMethod: checkout.deliveryOptions[checkout.deliveryMethod],
    };
}

function mapDispatch(dispatch) {
    return {
        actions: {
            cart: bindActionCreators(cartActions, dispatch)
        }
    };
}

