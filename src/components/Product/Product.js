import React, {Component} from 'react';

import './Product.css';

class Product extends Component {
    render() {

        let {image, name, unit, onBuyNow, onAddToCart} = this.props;
        return (
            <div className="product">
                <div className="product__image">
                    <img src={image}/>
                </div>
                <div className="product__name">{name} - <small>£{unit}</small></div>

                <div className="product__ctas">
                    <button className="button" onClick={onBuyNow}>Buy Now</button>
                    <button className="button" onClick={onAddToCart}>Add to Cart</button>
                </div>
            </div>
        );
    }
}

export default Product;
